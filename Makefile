# ascii -- interactive ASCII reference

VERS=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

CFLAGS = -O

PREFIX = /usr/local

.PHONY: all clean format cppcheck splint install uninstall
	version dist release refresh

all: ascii ascii.1

ascii: ascii.c splashscreen.h nametable.h
	$(CC) $(CFLAGS) -DREVISION='"$(VERS)"' ascii.c -o ascii

splashscreen.h: splashscreen
	sed <splashscreen >splashscreen.h \
		-e 's/\\/\\\\/g' \
		-e 's/"/\\"/g' \
		-e 's/.*/"&" "\\n"/'

nametable.h: nametable
	sed <nametable >nametable.h \
		-e '/^#/d' \
		-e 's/^[A-Za-z ]*: */    /' \
		-e 's/%%/    }, {/'

ascii.1: ascii.xml
	xmlto man ascii.xml

ascii.html: ascii.xml
	xmlto html-nochunks ascii.xml

clean:
	rm -f ascii ascii.o splashscreen.h nametable.h
	rm -f *.rpm *.tar.gz MANIFEST *.1 *.html

format:
	clang-format -verbose -i ascii.c

CPPCHECKOPTS =
cppcheck: nametable.h splashscreen.h
	cppcheck -DREVISION='"$(VERS)"' $(CPPCHECKOPTS) ascii.c

SPLINT_SUPPRESSIONS = -boolops -nullret -initallelements +charintliteral
splint: nametable.h splashscreen.h
	splint +quiet +posixlib $(SPLINT_SUPPRESSIONS) ascii.c

install: ascii ascii.1
	cp ascii $(DESTDIR)$(PREFIX)/bin/ascii
	cp ascii.1 $(DESTDIR)$(PREFIX)/share/man/man1

uninstall:
	rm $(DESTDIR)$(PREFIX)/bin/ascii
	rm $(DESTDIR)$(PREFIX)/share/man/man1/ascii.1

SOURCES = \
	README COPYING NEWS control Makefile \
	ascii.c ascii.xml splashscreen nametable

version:
	@echo $(VERS)

ascii-$(VERS).tar.gz: $(SOURCES) ascii.1
	@ls $(SOURCES) ascii.1 | sed s:^:ascii-$(VERS)/: >MANIFEST
	@(cd ..; ln -s ascii ascii-$(VERS))
	(cd ..; tar -czvf ascii/ascii-$(VERS).tar.gz `cat ascii/MANIFEST`)
	@(cd ..; rm ascii-$(VERS))

dist: ascii-$(VERS).tar.gz

release: ascii-$(VERS).tar.gz ascii.html
	shipper version=$(VERS) | sh -e -x

refresh: ascii.html
	shipper -N -w version=$(VERS) | sh -e -x
