/*
 * ascii.c -- quick crossreference for ASCII character aliases
 *
 * Tries to interpret arguments as names or aliases of ascii characters
 * and dumps out *all* the aliases of each. Accepts literal characters,
 * standard mnemonics, C-style backslash escapes, caret notation for control
 * characters, numbers in hex/decimal/octal/binary, English names.
 *
 * The slang names used are selected from the 2.2 version of the USENET ascii
 * pronunciation guide.  Some additional ones were merged in from the Jargon
 * File.
 *
 * For license terms, see the file COPYING.
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <getopt.h>
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef S_SPLINT_S
#include <ctype.h>
#include <unistd.h>
#endif

#define MODE_HELP (1 << 0)
#define MODE_VERSION (1 << 1)
#define MODE_BTABLE (1 << 2)
#define MODE_OTABLE (1 << 3)
#define MODE_DTABLE (1 << 4)
#define MODE_XTABLE (1 << 5)
#define MODE_CHARS (1 << 6)

#define MASK_MARKED (1 << 0)

#define MASK_CNTRL (1 << 1)
#define MASK_PUNCT (1 << 2)
#define MASK_UPPER (1 << 3)
#define MASK_LOWER (1 << 4)
#define MASK_DIGIT (1 << 5)
#define MASK_XALPHA (1 << 6)

#define MASK_ALPHA (MASK_UPPER | MASK_LOWER)
#define MASK_ALNUM (MASK_UPPER | MASK_LOWER | MASK_DIGIT)
#define MASK_PRINT (MASK_UPPER | MASK_LOWER | MASK_DIGIT | MASK_PUNCT)
#define MASK_XDIGIT (MASK_DIGIT | MASK_XALPHA)

struct char_class {
    const char *name;
    unsigned int mask;
};

static char *progname;
static bool vertical = false;
static bool terse = false;
static bool literal = false;

static const char *cnames[128][16] = { {
#include "nametable.h"
} };

static const char splashscreen[] =
#include "splashscreen.h"
    ;

/* The character class names are from POSIX. */
static const struct char_class char_classes[] = {
    { "alnum", MASK_ALNUM },
    { "alpha", MASK_ALPHA },
    { "cntrl", MASK_CNTRL },
    { "digit", MASK_DIGIT },
    { "lower", MASK_LOWER },
    { "print", MASK_PRINT },
    { "punct", MASK_PUNCT },
    { "upper", MASK_UPPER },
    { "xdigit", MASK_XDIGIT },
    { NULL, 0 },
};

static unsigned char char_mask_table[256];

static bool str_case_equal(const char *s, const char *t)
{
    while (tolower(*s) == tolower(*t)) {
        if (*s == '\0') {
            return true;
        }
        s++;
        t++;
    }
    return false;
}

static unsigned int char_class_mask(const char *name)
{
    const struct char_class *cc;

    for (cc = char_classes; cc->name; cc++) {
        if (str_case_equal(cc->name, name))
            return cc->mask;
    }
    return 0;
}

static void init_char_mask_table(void)
{
    unsigned int ch;

    ch = 0;
    while (ch < 0x20)
        char_mask_table[ch++] = MASK_CNTRL;
    char_mask_table[ch++] = 0; /* Space */
    while (ch < 0x30)
        char_mask_table[ch++] = MASK_PUNCT;
    while (ch < 0x3a)
        char_mask_table[ch++] = MASK_DIGIT;
    while (ch < 0x41)
        char_mask_table[ch++] = MASK_PUNCT;
    while (ch < 0x47)
        char_mask_table[ch++] = MASK_UPPER | MASK_XALPHA;
    while (ch < 0x5b)
        char_mask_table[ch++] = MASK_UPPER;
    while (ch < 0x61)
        char_mask_table[ch++] = MASK_PUNCT;
    while (ch < 0x67)
        char_mask_table[ch++] = MASK_LOWER | MASK_XALPHA;
    while (ch < 0x7b)
        char_mask_table[ch++] = MASK_LOWER;
    while (ch < 0x7f)
        char_mask_table[ch++] = MASK_PUNCT;
    char_mask_table[ch] = MASK_CNTRL; /* Delete */
}

static bool mark_mask(unsigned int mask)
{
    unsigned int ch;
    bool match = false;

    for (ch = 0; ch < 128; ch++) {
        if (char_mask_table[ch] & mask) {
            char_mask_table[ch] |= MASK_MARKED;
            match = true;
        }
    }
    return match;
}

static bool mark(unsigned int ch)
{
    if (ch >= 256) {
        return false;
    }
    char_mask_table[ch] |= MASK_MARKED;
    return true;
}

static unsigned int control_to_caret(unsigned int ch)
{
    if (ch < 0x20) {
        return '@' + ch;
    }
    if (ch == 0x7f) {
        return '?';
    }
    return UINT_MAX;
}

static unsigned int caret_to_control(unsigned int ch)
{
    if ((ch >= '@') && (ch < '@' + 0x20)) {
        return ch - '@';
    }
    if (ch == '?') {
        return 0x7f;
    }
    return UINT_MAX;
}

static void format_binary(char *buf, size_t bufsize, unsigned int val)
{
    char *p = buf + bufsize;

    if (p > buf) {
        *--p = '\0';
    }
    while (p > buf) {
        *--p = (val & 1) ? '1' : '0';
        val >>= 1;
    }
}

static unsigned int parse_partial_digits(const char **sp, unsigned int radix)
{
    const char digits[] = "0123456789abcdef";
    const char *digitp;
    const char *s;
    unsigned int digit, val;

    s = *sp;
    if (*s == '\0') {
        return UINT_MAX;
    }
    val = 0;
    for (; *s != '\0'; s++) {
        if ((digitp = strchr(digits, tolower(*s))) == NULL) {
            break;
        }
        if ((digit = digitp - digits) >= radix) {
            break;
        }
        val = (val * radix) + digit;
        if (val >= 256) {
            val = UINT_MAX;
            break;
        }
    }
    *sp = s;
    return val;
}

static unsigned int parse_digits(const char *s, unsigned int radix)
{
    unsigned int val;

    val = parse_partial_digits(&s, radix);
    return (*s == '\0') ? val : UINT_MAX;
}

static unsigned int parse_radix_letter_and_digits(const char *s)
{
    switch (s[0]) {
    case 'b':
        return parse_digits(&s[1], 2);
    case 'o':
        return parse_digits(&s[1], 8);
    case 'd':
        return parse_digits(&s[1], 10);
    case 'x':
        return parse_digits(&s[1], 16);
    default:
        return UINT_MAX;
    }
}

static unsigned int parse_row_slash_column(const char *s)
{
    unsigned int hi, lo;

    if ((hi = parse_partial_digits(&s, 10)) == UINT_MAX)
        return UINT_MAX;
    if ((hi >= 16) || (*s != '/'))
        return UINT_MAX;
    if ((lo = parse_digits(&s[1], 10)) == UINT_MAX)
        return UINT_MAX;
    if (lo >= 16)
        return UINT_MAX;
    return 16 * hi + lo;
}

static bool input_row_slash_column(const char *s)
{
    return mark(parse_row_slash_column(s));
}

static bool input_numeric_codepoint(const char *s)
{
    bool match;

    if (mark(parse_radix_letter_and_digits(s))) {
        return true;
    }
    if ((s[0] == '0') || (s[0] == '\\')) {
        if (mark(parse_radix_letter_and_digits(&s[1]))) {
            return true;
        }
    }

    match = false;
    if (s[0] == '0') {
        if (mark(parse_digits(&s[1], 8))) {
            match = true;
        }
    }
    if (mark(parse_digits(s, 2))) {
        match = true;
    }
    if (mark(parse_digits(s, 8))) {
        match = true;
    }
    if (mark(parse_digits(s, 10))) {
        match = true;
    }
    if (mark(parse_digits(s, 16))) {
        match = true;
    }
    return match;
}

static bool input_caret_notation(const char *s)
{
    if (strlen(s) != 2) {
        return false;
    }
    if (s[0] != '^') {
        return false;
    }
    return mark(caret_to_control(s[1]));
}

static bool input_character_name(const char *s)
{
    char buf[32];
    const char **names;
    const char *name;
    char *p;
    unsigned int ch;

    if (strlen(s) >= sizeof(buf)) {
        return false;
    }
    snprintf(buf, sizeof(buf), "%s", s);

    /* map dashes and other junk to spaces */
    for (p = buf; *p; p++) {
        if ((*p == '-') || isspace(*p)) {
            *p = ' ';
        }
    }

    /* strip "sign" suffix */
    if (strlen(buf) > strlen("sign")) {
        p = strchr(buf, '\0') - strlen("sign");
        if (str_case_equal(p, "sign")) {
            *p = '\0';
        }
    }

    for (ch = 0; ch < 128; ch++) {
        for (names = cnames[ch]; (name = *names); names++) {
            if (str_case_equal(name, buf)) {
                return mark(ch);
            }
        }
    }
    return false;
}

static bool input_character_class_name(const char *s)
{
    return mark_mask(char_class_mask(s));
}

static bool input(const char *s)
{
    bool match = false;

    if (input_row_slash_column(s)) {
        match = true;
    }
    if (input_numeric_codepoint(s)) {
        match = true;
    }
    if (input_caret_notation(s)) {
        match = true;
    }
    if (input_character_name(s)) {
        match = true;
    }
    if (input_character_class_name(s)) {
        match = true;
    }
    if (strlen(s) == 1) {
        if (mark(s[0])) {
            match = true;
        }
    }
    return match;
}

static bool input_literal_characters(const char *s)
{
    for (; *s; s++) {
        if (!mark(*s)) {
            return false;
        }
    }
    return true;
}

static void print_character_names(const char **names)
{
    const char note_prefix[] = "# ";
    const char *note = NULL;
    const char *name;
    bool first = true;

    name = *names++;
    printf("Official name: %s\n", name);
    if ((name = names[0]) == NULL) {
        return;
    }
    if (name[0] == '\\') {
        printf("C escape: '%s'\n", name);
        names++;
    }
    while ((name = *names++)) {
        if (!strncmp(name, note_prefix, strlen(note_prefix))) {
            note = &name[strlen(note_prefix)];
        } else if (first) {
            printf("Other names: %s", name);
            first = false;
        } else {
            printf(", %s", name);
        }
    }
    putchar('\n');
    if (note) {
        printf("Note: %s\n", note);
    }
}

static void print_control_character_names(const char **names)
{
    const char *name;

    while ((name = *names)) {
        if (strlen(name) < 4 && isupper(name[0])) {
            printf(", %s", name);
            names++;
        } else {
            break;
        }
    }
    putchar('\n');
    print_character_names(names);
}

static void print_character(unsigned int ch)
{
    static bool any_printed = false;
    const char **names = cnames[ch];
    unsigned int caret;
    char binary[8 + 1];

    if (any_printed) {
        putchar('\n');
    }
    any_printed = true;
    format_binary(binary, sizeof(binary), ch);
    if (terse) {
        printf("%u/%u", ch / 16, ch % 16);
        printf("   %u", ch);
        printf("   0x%02X", ch);
        printf("   0o%o", ch);
        printf("   %s\n", binary);
        return;
    }
    printf("ASCII %u/%u is", ch / 16, ch % 16);
    printf("decimal %03u, ", ch);
    printf("hex %02x, ", ch);
    printf("octal %03o, ", ch);
    printf("bits %s: ", binary);
    if (ch & 0x80) { /* high-half character */
        ch &= ~0x80;
        if ((caret = control_to_caret(ch)) != UINT_MAX) {
            printf("meta-^%c\n", caret);
        } else {
            printf("meta-%c\n", ch);
        }
    } else if ((caret = control_to_caret(ch)) != UINT_MAX) {
        printf("called ^%c", caret);
        print_control_character_names(names);
    } else {
        printf("prints as `%s'\n", *names++);
        print_character_names(names);
    }
}

static void print_table(unsigned int radix)
{
    unsigned int i, j, ch, len, rows, cols;
    const char separator[] = "   ";
    const char *tail = separator + 3;
    const char *space;
    const char *name;
    char binary[8 + 1];

    if (vertical)
        cols = 4;
    else
        cols = 8;
    rows = 128 / cols;
    for (i = 0; i < rows; i++) {
        for (j = 0; j < cols; j++) {
            ch = j * rows + i;
            name = cnames[ch][0];
            len = strlen(name);
            space = tail - (len % 3);
            switch (radix) {
            case 10:
                printf("%5d %1s%1s", ch, name, space);
                break;
            case 8:
                printf("  %03o %1s%1s", ch, name, space);
                break;
            case 16:
                printf("   %02X %1s%1s", ch, name, space);
                break;
            case 2:
                format_binary(binary, sizeof(binary), ch);
                printf("   %s %1s%1s", binary, name, space);
                break;
            }
        }
        printf("\n");
    }
}

static void generic_usage(FILE *out, int status)
{
    fprintf(out, "Usage: %s [-adxohv] [-t] [char-alias...]\n", progname);
    fputs(splashscreen, out);
    exit(status);
}

static void usage(void) { generic_usage(stderr, 2); }

static unsigned int parse_command_line(int argc, char **argv)
{
    const char *arg;
    unsigned int mode;
    int op;

    mode = 0;
    progname = argv[0];
    while ((op = getopt(argc, argv, "abdhostvVx")) != -1) {
        switch (op) {
        case 'a':
            vertical = true;
            break;
        case 'b':
            mode |= MODE_BTABLE;
            break;
        case 'd':
            mode |= MODE_DTABLE;
            break;
        case 'h':
            mode |= MODE_HELP;
            break;
        case 'o':
            mode |= MODE_OTABLE;
            break;
        case 's':
            terse = literal = true;
            break;
        case 't':
            terse = true;
            break;
        case 'v':
        case 'V':
            mode |= MODE_VERSION;
            break;
        case 'x':
            mode |= MODE_XTABLE;
            break;
        case '?':
            usage();
            break;
        default:
            usage();
            break;
        }
    }
    if (mode & MODE_HELP) {
        return MODE_HELP;
    }
    if (mode & MODE_VERSION) {
        return MODE_VERSION;
    }
    if (mode && (terse || literal)) {
        usage();
    }
    switch (mode) {
    case 0:
        break;
    case MODE_BTABLE:
        return mode;
    case MODE_OTABLE:
        return mode;
    case MODE_DTABLE:
        return mode;
    case MODE_XTABLE:
        return mode;
    default:
        usage();
    }
    if (vertical) {
        usage();
    }
    if (argc == optind) {
        return MODE_HELP;
    }
    for (argv = argv + optind; (arg = *argv); argv++) {
        if (!arg[0]) {
            fprintf(stderr, "blank input argument given\n");
            exit(1);
        }
        if (literal && !input_literal_characters(arg)) {
            fprintf(stderr, "contains non-ASCII character: %s\n", arg);
            exit(1);
        }
        if (!literal && !input(*argv)) {
            fprintf(stderr, "unknown character: %s\n", arg);
            exit(1);
        }
    }
    return MODE_CHARS;
}

int main(int argc, char **argv)
{
    unsigned int ch;

    init_char_mask_table();
    switch (parse_command_line(argc, argv)) {
    case MODE_CHARS:
        for (ch = 0; ch < 128; ch++) {
            if (char_mask_table[ch] & MASK_MARKED) {
                print_character(ch);
            }
        }
        break;
    case MODE_BTABLE:
        print_table(2);
        break;
    case MODE_OTABLE:
        print_table(8);
        break;
    case MODE_DTABLE:
        print_table(10);
        break;
    case MODE_XTABLE:
        print_table(16);
        break;
    case MODE_VERSION:
        printf("ascii %s\n", REVISION);
        break;
    case MODE_HELP:
        generic_usage(stdout, 0);
        break;
    default:
        usage();
    }
    return 0;
}
